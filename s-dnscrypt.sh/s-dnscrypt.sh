#!/bin/sh

#2018-12-01 11:58 WIB GMT+7
#version 1.0

#backup previous dns and change current dns
chgDns(){
 cp /etc/resolv.conf /etc/resolv.conf.bak
 echo nameserver $1 > /etc/resolv.conf
 echo 'succesfull change dns.';
}

#remove current dns and restore from backup if present
resDns(){
 if [ -f /etc/resolv.conf.bak ]; then
  rm /etc/resolv.conf
  mv /etc/resolv.conf.bak /etc/resolv.conf
  echo 'dns restored.';
 else
  echo 'file not found.'
 fi
}

#start dnscrypt and call chgDns()
startDnscrypt(){
 sudo dnscrypt-proxy -a $1 -R $2 -d
 if [ $3 = "l" ]; then
  chgDns $1
 fi
 echo 'dnscrypt started.';
}

#stop dnscrypt and call resDns()
killDnscrypt(){
 pkill dnscrypt-proxy
 resDns
 echo 'dnscrypt stoped.';
}

#test whether script run with root privilage
isRoot(){
  if [ $(whoami) = "root" ] ; then
    rootPass=true
  elif [ $(whoami) != "root" ] && [ $rootPass = false ] ; then
    rootPass=true
    sudo $(pwd)/dnscrypt-backend.sh $@          #rerun this script with sudo
    exit                                        #exit to prevent double execution of this script
  fi
}

#exec spesific command based on args
execArg(){
 local="127.0.0.2"    #ip for localhost (if '-a' spesificied)
 ipadd=$(hostname -I)    #ip local lan for "shared" feature (if '-s' spesified)
 nets=""    #(string) to identify whether start dnscrypt for 'local' or 'share'

 #args for help section
 if [ $1 = "-h" ] || [ $1 = "--help" ]; then
  echo "usage: dnscrypt-backend (h|s) (a|m) (ip if use -m) (resolver)";
  echo "-l : local dns (host).";
  echo "-s : use local ip as dns.";
  echo "-a : automatic ip.";
  echo "-m : manual ip.\n";
  echo "resolver : \ncloudflare \ncloudflare-ipv6 \ncommons-host \naaflalo-me \naaflalo-me-gcp.";
  exit
 fi

 #args for kill dnscrypt
 if [ $1 = "-k" ]; then
  killDnscrypt
  exit
 fi

 #args for start dnscrypt
 if [ $1 = "-l" ] && [ $2 = "-a" ]; then
  nets="l"
  startDnscrypt $local $3 $nets
 elif [ $1 = "-l" ] && [ $2 = "-m" ]; then
  nets="l"
  startDnscrypt $3 $4 $nets
 elif [ $1 = "-s" ] && [ $2 = "-a" ]; then
  nets="s"
  startDnscrypt $ip $3 $nets
 elif [ $1 = "-s" ] && [ $2 = "-m" ]; then
  nets="s"
  startDnscrypt $3 $4 $nets
 else
  echo 'wrong argument'.    #print error if 'no match argument' or 'argument not found'
 fi
}

#main function
main(){
 rootPass=false    #(boolean) var for show privilage status
 isRoot $@ $rootPass
 execArg $@
 exit
}

#call main()
main $@
